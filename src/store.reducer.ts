import{useMemo, useReducer, Reducer} from "react";
import {
    ForestResponse,
    rootHierarchySelector,
    viewStateSelector,
    mapUndefined,
    ViewState
} from "@vdimensions/forest-js-frontend";
import { EMPTY_FOREST_RESPONSE, NoopStore } from "./store";
import { Map } from "@vdimensions/forest-js-frontend/common-types";

type ViewStateMap = { [id: string] : ViewState }
type ViewStateKeyMap = { [id: string] : boolean }

const reducerMethod = (state: ForestResponse, data: ForestResponse) => {
    if (data.path === state.path) {
        const mergedViewStates: ViewStateMap = { };
        const viewStateKeys: ViewStateKeyMap = { };
        Object.keys(state.views).forEach((key: string) => {
            mergedViewStates[key] = state.views[key];
            viewStateKeys[key] = false;
        });
        Object.keys(data.views).forEach((key: string) => {
            const vs = data.views[key];
            const existing = mergedViewStates[key];
            if (existing) {
                mergedViewStates[key] = new ViewState({
                    ...existing,
                    model: {...existing.model, ...vs.model},
                    regions: {...existing.regions, ...vs.regions},
                    commands: {...existing.commands, ...vs.commands }
                });
            } else {
                mergedViewStates[key] = vs;
            }
            viewStateKeys[key] = true;
        });
        const views : Map<ViewState> = {};
        // delete viewstates for views no longer returned by the server
        Object.keys(viewStateKeys).forEach(k => {
            if (viewStateKeys[k]) {
                views[k] = mergedViewStates[k];
            }
        });
        return new ForestResponse({
            ...state,
            shell: data.shell,
            views
        });
    } else {
        return data;
    }
};

export const useForestReducerStore = () => {
    const [state, dispatch] = useReducer<Reducer<ForestResponse, ForestResponse>>(reducerMethod, EMPTY_FOREST_RESPONSE);

    const selectors = useMemo(() => {
        return {
            useRootHierarchy: () => {
                const data = (mapUndefined(rootHierarchySelector))(state);
                return data || NoopStore.useRootHierarchy();
            },
            useViewState: (instanceId: string) => {
                const map1 = viewStateSelector(instanceId);
                const data = (mapUndefined(map1))(state);
                return data || NoopStore.useViewState(instanceId);
            }
        }
    }, [state]);
    return {
        useDispatch: () => dispatch,
        ...selectors
    }
}